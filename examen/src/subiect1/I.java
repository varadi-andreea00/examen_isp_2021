package subiect1;

public interface I {

}

class E implements I {
    H h;
    F f;

    E() {
        f = new F();
    }

    E(H h) {
        this.h = h;
    }

    public void metG(int i) {

    }
}

class F {
    public void metA() {

    }
}

class H {

}

class G {
    public void i() {

    }
}

class B extends G {
    private long t;

    public void x() {

    }
}

class D {
    void metB(B b) {

    }
}

