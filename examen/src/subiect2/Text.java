package subiect2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Text extends JFrame implements ActionListener {
    JFrame frame = new JFrame();
    JTextField textField1 = new JTextField();
    JTextField textField2 = new JTextField();
    JButton moveTextButton = new JButton();
    String PREDEFINED_TEXT = "Hello user";
    int numberOfClicks = 0;

    Text() {

        moveTextButton.setText("Move Text");
        moveTextButton.setBounds(100, 260, 200, 40);
        textField1.setBounds(20, 50, 200, 50);
        textField2.setBounds(20, 150, 200, 50);
        textField1.setEditable(false);
        textField2.setEditable(false);

        moveTextButton.addActionListener(this);
        frame.add(textField1);
        frame.add(textField2);
        frame.add(moveTextButton);
        frame.setTitle("Text app");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(420, 420);
        frame.setLayout(null);
        frame.setVisible(true);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        numberOfClicks++;
        if ((e.getSource() == moveTextButton) && (numberOfClicks % 2 == 1)) {
            textField1.setText(PREDEFINED_TEXT);
            textField2.setText("");
        } else if ((e.getSource() == moveTextButton) && (numberOfClicks % 2 == 0)) {
            textField1.setText("");
            textField2.setText(PREDEFINED_TEXT);
        }
    }

    public static void main(String[] args) {
        Text text = new Text();
    }
}
